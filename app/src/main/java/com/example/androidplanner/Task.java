package com.example.androidplanner;

import java.util.ArrayList;
import java.util.List;

public class Task {
    List<Task> taskList = new ArrayList<>(); // Объявление и инициализация списка задач
    public static final String[] priorities = {"Высокий", "Средний", "Низкий"};

    private String title;
    private String description;
    private String priority;

    public Task(String title, String description, String priority) {
        this.title = title;
        this.description = description;
        this.priority = priority;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getPriority() {
        return priority;
    }
}
