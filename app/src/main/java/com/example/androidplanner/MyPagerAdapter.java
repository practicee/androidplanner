package com.example.androidplanner;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.viewpager2.adapter.FragmentStateAdapter;

public class MyPagerAdapter extends FragmentStateAdapter {

    public MyPagerAdapter(FragmentActivity fragmentActivity) {
        super(fragmentActivity);
    }

    @Override
    public Fragment createFragment(int position) {
        // Создаем фрагмент для каждого дня недели
        switch (position) {
            case 0:
                return new MondayFragment();
            case 1:
                return new TuesdayFragment();
            case 2:
                return new WednesdayFragment();
            case 3:
                return new ThursdayFragment();
            case 4:
                return new FridayFragment();
            case 5:
                return new SaturdayFragment();
            case 6:
                return new SundayFragment();
            default:
                return null;
        }
    }

    @Override
    public int getItemCount() {
        // Указываем количество страниц (фрагментов), равное количеству дней недели
        return 7; // Например, для 7 дней недели
    }
}
