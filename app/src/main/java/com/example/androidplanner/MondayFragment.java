package com.example.androidplanner;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class MondayFragment extends Fragment {
    List<Task> taskList = new ArrayList<>();
    private RecyclerView recyclerView;

    private TaskAdapter taskAdapter; // Создайте свой адаптер для задач

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_monday, container, false);

        // Находим RecyclerView в макете
        recyclerView = view.findViewById(R.id.recycler_view_monday_tasks);

        // Создаем адаптер для RecyclerView
        taskAdapter = new TaskAdapter(taskList); // Создайте свой адаптер для задач

        // Устанавливаем менеджер компоновки (layout manager) для RecyclerView
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        // Устанавливаем адаптер для RecyclerView
        recyclerView.setAdapter(taskAdapter);

        return view;
    }
}

