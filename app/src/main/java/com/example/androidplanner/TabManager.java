package com.example.androidplanner;

import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;

public class TabManager {
    private TabLayout tabLayout;

    public TabManager(TabLayout tabLayout) {
        this.tabLayout = tabLayout;
    }

    public void setupWithViewPager(ViewPager viewPager) {
        tabLayout.setupWithViewPager(viewPager);
    }

    public void addTab(String tabTitle) {
        tabLayout.addTab(tabLayout.newTab().setText(tabTitle));
    }

    public void setOnTabSelectedListener(TabLayout.OnTabSelectedListener listener) {
        tabLayout.addOnTabSelectedListener(listener);
    }
}
