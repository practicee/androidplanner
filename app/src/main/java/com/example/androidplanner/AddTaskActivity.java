package com.example.androidplanner;

import static com.example.androidplanner.Task.priorities;

import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import androidx.appcompat.app.AppCompatActivity;

public class AddTaskActivity extends AppCompatActivity {

    private EditText editTextTitle;
    private EditText editTextDescription;
    private Spinner spinnerPriority;
    private Button buttonAddTask;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_task_layout);

        editTextTitle = findViewById(R.id.editTextTitle);
        editTextDescription = findViewById(R.id.editTextDescription);
        spinnerPriority = findViewById(R.id.spinnerPriority);
        buttonAddTask = findViewById(R.id.buttonAddTask);

        // Устанавливаем адаптер для спиннера с приоритетами
        ArrayAdapter<String> priorityAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, Task.priorities);
        priorityAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerPriority.setAdapter(priorityAdapter);

        buttonAddTask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Получаем значения из полей ввода
                String title = editTextTitle.getText().toString();
                String description = editTextDescription.getText().toString();
                String priority = spinnerPriority.getSelectedItem().toString();

                // Создаем объект задачи
                Task newTask = new Task(title, description, priority);

                // Сохраняем задачу в базе данных или другом хранилище
                // Например, вызов метода для сохранения задачи в базе данных

                // После сохранения задачи, можно вернуться на предыдущий экран
                finish();
            }
        });
    }
}
