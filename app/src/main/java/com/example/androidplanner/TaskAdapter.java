package com.example.androidplanner;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class TaskAdapter extends RecyclerView.Adapter<TaskAdapter.TaskViewHolder> {
    private List<Task> tasks;

    public TaskAdapter(List<Task> tasks) {
        this.tasks = tasks;
    }

    // Внутренний класс ViewHolder для элементов списка
    public class TaskViewHolder extends RecyclerView.ViewHolder {
        TextView taskTitle;
        TextView taskPriority;

        public TaskViewHolder(View itemView) {
            super(itemView);
            taskTitle = itemView.findViewById(R.id.task_title);
            taskPriority = itemView.findViewById(R.id.task_priority);
        }
    }

    @NonNull
    @Override
    public TaskViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.task_item, parent, false);
        return new TaskViewHolder(view);
    }

    @Override
    public int getItemCount() {
        return tasks.size(); // Возвращает количество задач в списке
    }

    @Override
    public void onBindViewHolder(@NonNull TaskViewHolder holder, int position) {
        Task task = tasks.get(position); // Получает задачу по позиции
        holder.taskTitle.setText(task.getTitle());
        holder.taskPriority.setText(task.getPriority());
    }

}
