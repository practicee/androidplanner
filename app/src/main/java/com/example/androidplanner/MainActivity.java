package com.example.androidplanner;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager2.widget.ViewPager2;
import com.google.android.material.tabs.TabLayout;
import com.google.android.material.tabs.TabLayoutMediator;

public class MainActivity extends AppCompatActivity {

    private ViewPager2 viewPager;
    private MyPagerAdapter pagerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        viewPager = findViewById(R.id.viewPager);

        // Создаем адаптер для ViewPager2
        pagerAdapter = new MyPagerAdapter(this);

        // Устанавливаем адаптер для ViewPager2
        viewPager.setAdapter(pagerAdapter);

        // Находим TabLayout в макете
        TabLayout tabLayout = findViewById(R.id.tabLayout);

        // Привязываем TabLayout к ViewPager2
        new TabLayoutMediator(tabLayout, viewPager, (tab, position) -> {
            // Устанавливаем текст вкладки (таба)
            tab.setText("День " + (position + 1)); // Например, "День 1", "День 2" и так далее
        }).attach();
    }
}
